import csv

summer = []
autumn = []
winter = []
spring = []

with open('data.csv', 'r') as c:
    r = csv.reader(c)
    for row in r:
        try:
            month = int(row[3]) # Months are indexed at 1
        except ValueError:
            continue

        if not row[5]:
            continue

        if month in [12, 1, 2]:
            summer.append(row[5])
        elif month in [3, 4, 5]:
            autumn.append(row[5])
        elif month in [6, 7, 8]:
            winter.append(row[5])
        else:
            spring.append(row[5])

print('---- SUMMER ----')
[print(i) for i in summer]

print('---- AUTUMN ----')
[print(i) for i in autumn]

print('---- WINTER ----')
[print(i) for i in winter]

print('---- SPRING ----')
[print(i) for i in spring]

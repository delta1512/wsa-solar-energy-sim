# Western Sydney Airport Simulator

A simulator to estimate the solar electricity system requirements for the new Western Sydney Airport. This is the project repository for Professional Experience group PS2128 for Spring 2021 at Western Sydney University.